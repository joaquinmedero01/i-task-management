package com.jmedero.ITaskManagement.domain.Task;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "tasks")
@Data
public class Task {
    @Id
    private String id;
    private String taskId;
    private String description;
}
