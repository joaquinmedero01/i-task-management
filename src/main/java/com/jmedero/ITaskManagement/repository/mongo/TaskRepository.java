package com.jmedero.ITaskManagement.repository.mongo;

import com.jmedero.ITaskManagement.domain.Task.Task;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TaskRepository extends MongoRepository<Task, String> {
    Task findByTaskId(String taskId);
}
