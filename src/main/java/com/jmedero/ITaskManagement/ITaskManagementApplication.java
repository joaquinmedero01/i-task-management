package com.jmedero.ITaskManagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ITaskManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ITaskManagementApplication.class, args);
	}

}
