package com.jmedero.ITaskManagement.services;

import com.jmedero.ITaskManagement.domain.Task.Task;
import com.jmedero.ITaskManagement.repository.mongo.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    public Task createTask(Task task) {
         return taskRepository.save(task);
    }

    public Task getTaskById(String taskId) {
        return taskRepository.findByTaskId(taskId);
    }
}
